<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/categories", name="get_categories", methods={"GET"})
     */
    public function getCollection(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/CategoryController.php',
        ]);
    }

    /**
     * @Route("/categories", name="post_category", methods={"POST"})
     */
    public function create()
    {
    }

    /**
     * @Route("/categories/{id}", name="get_category", methods={"GET"})
     */
    public function getItem()
    {
    }

    /**
     * @Route("/categories/{id}", name="put_category", methods={"PUT"})
     */
    public function update()
    {
    }

    /**
     * @Route("/categories/{id}", name="delete_category", methods={"DELETE"})
     */
    public function delete()
    {
    }
}
