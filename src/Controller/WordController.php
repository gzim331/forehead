<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WordController extends AbstractController
{
    /**
     * @Route("/words", name="get_words", methods={"GET"})
     */
    public function getCollection(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/WordController.php',
        ]);
    }

    /**
     * @Route("/words", name="post_word", methods={"POST"})
     */
    public function create()
    {
    }

    /**
     * @Route("/words/{id}", name="get_word", methods={"GET"})
     */
    public function getItem()
    {
    }

    /**
     * @Route("/words/{id}", name="put_word", methods={"PUT"})
     */
    public function update()
    {
    }

    /**
     * @Route("/words/{id}", name="delete_word", methods={"DELETE"})
     */
    public function delete()
    {
    }
}
